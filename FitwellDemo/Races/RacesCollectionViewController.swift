//
//  ViewController.swift
//  FitwellDemo
//
//  Created by Konstantinos Chatzigeorgiou on 08/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit
import CoreData

// MARK: - Class

class RacesCollectionViewController: UICollectionViewController {
    
    // MARK: - Properties
    
    let managedObjectContext = CoreDataStack.sharedStack.managedObjectContext
    let fetchRequest: NSFetchRequest<Race> = Race.fetchRequest()
    var fetchedResultsController: NSFetchedResultsController<Race>?
    var selectedRace: Race?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sortDescriptor = NSSortDescriptor(key: #keyPath(Race.createdDate), ascending: false)
        self.fetchRequest.sortDescriptors = [sortDescriptor]
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: self.fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchedResultsController?.delegate = self
        
        do {
            try self.fetchedResultsController?.performFetch()
        } catch let e {
            print("\(e)")
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowRace" {
            let raceViewController = segue.destination as? RaceViewController
            raceViewController?.race = self.selectedRace
        }
    }
    
    // MARK: - Methods
    
    func configureCell(cell: RaceCollectionViewCell, for indexPath: IndexPath) {
        if let race = self.fetchedResultsController?.object(at: indexPath) {
            cell.layoutIfNeeded()
            
            cell.avatarImageView.layer.cornerRadius = cell.avatarImageView.frame.width / 2
            
            if let backgroundImageDate = race.backgroundImage {
                cell.backgroundImageView.image = UIImage(data: backgroundImageDate)
            }
            
            if let userAvatarData = race.user?.avatar {
                cell.avatarImageView.image = UIImage(data: userAvatarData)
            }
            
            cell.titleLabel.text = race.title
            cell.totalTimeLabel.text = race.totalTime
            cell.participantsLabel.text = "\(race.participants)"
            cell.totalKmLabel.text = "\(race.totalKm) KM"
        }
    }
    
}

// MARK: - UICollectionViewDataSource

extension RacesCollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.fetchedResultsController?.sections?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RaceCell", for: indexPath) as! RaceCollectionViewCell
        self.configureCell(cell: cell, for: indexPath)
        return cell
    }
    
}

// MARK: - UICollectionViewDataSource

extension RacesCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedRace = self.fetchedResultsController?.object(at: indexPath)
        self.performSegue(withIdentifier: "ShowRace", sender: self)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension RacesCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var insets: CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            insets = self.view.safeAreaInsets.top + self.view.safeAreaInsets.bottom
        }
        
        return CGSize(width: collectionView.frame.size.width, height: (collectionView.frame.size.height - insets) / 3)
    }
    
}

// MARK: - NSFetchedResultsControllerDelegate -

extension RacesCollectionViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        let sectionIndexSet = IndexSet(integer: sectionIndex)
        
        switch type {
        case .insert:
            self.collectionView?.insertSections(sectionIndexSet)
        case .delete:
            self.collectionView?.deleteSections(sectionIndexSet)
        default:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let newIndexPath = newIndexPath {
                self.collectionView?.insertItems(at: [newIndexPath])
            }
        case .delete:
            if let indexPath = indexPath {
                self.collectionView?.deleteItems(at: [indexPath])
            }
        case .update:
            if let indexPath = indexPath, let cell = self.collectionView?.cellForItem(at: indexPath) as? RaceCollectionViewCell {
                self.configureCell(cell: cell, for: indexPath)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                self.collectionView?.deleteItems(at: [indexPath])
                self.collectionView?.insertItems(at: [newIndexPath])
            }
        }
    }
    
}
