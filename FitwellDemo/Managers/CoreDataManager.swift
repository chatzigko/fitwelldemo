//
//  CoreDataManager.swift
//  FitwellDemo
//
//  Created by Konstantinos Chatzigeorgiou on 08/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    // MARK: - Properties
    
    static let shared = CoreDataManager()
    let managedObjectContext = CoreDataStack.sharedStack.managedObjectContext
    
    // MARK: - Methods
    
    func createTestData() {
        var user: User?
        
        if let userEntityDescription = NSEntityDescription.entity(forEntityName: "User", in: self.managedObjectContext) {
            user = User(entity: userEntityDescription, insertInto: self.managedObjectContext)
            user?.firstName = "Adriana"
            user?.lastName = "Lima"
            
            if let img = UIImage(named: "profile.png") {
                user?.avatar = UIImagePNGRepresentation(img)
            }
        }
        
        var timeline: Timeline?
        
        if let timelineItemEntityDescription = NSEntityDescription.entity(forEntityName: "TimelineItem", in: self.managedObjectContext) {
            let timelineItem1 = TimelineItem(entity: timelineItemEntityDescription, insertInto: managedObjectContext)
            timelineItem1.type = TransportType.bus.rawValue
            timelineItem1.desc = "It was some time before he obtained any answer, and the reply, when made, was unpropitious."
            timelineItem1.time = "18:16"
            
            let timelineItem2 = TimelineItem(entity: timelineItemEntityDescription, insertInto: self.managedObjectContext)
            timelineItem2.type = TransportType.train.rawValue
            timelineItem2.desc = "It was some time before he obtained any answer."
            timelineItem2.time = "20:20"
            
            let timelineItem3 = TimelineItem(entity: timelineItemEntityDescription, insertInto: self.managedObjectContext)
            timelineItem3.type = TransportType.airplane.rawValue
            timelineItem3.desc = "It was some time before he obtained any answer."
            timelineItem3.time = "22:35"
            
            if let timelineEntityDescription = NSEntityDescription.entity(forEntityName: "Timeline", in: self.managedObjectContext) {
                timeline = Timeline(entity: timelineEntityDescription, insertInto: self.managedObjectContext)
                timeline?.addToItems(timelineItem1)
                timeline?.addToItems(timelineItem2)
                timeline?.addToItems(timelineItem3)
            }
        }
        
        if let raceEntityDescription = NSEntityDescription.entity(forEntityName: "Race", in: self.managedObjectContext) {
            let race1 = Race(entity: raceEntityDescription, insertInto: self.managedObjectContext)
            
            if let img = UIImage(named: "bg.jpeg") {
                race1.backgroundImage = UIImagePNGRepresentation(img)
            }
            
            race1.title = "Some awesome place 1"
            race1.totalTime = "1d 12h"
            race1.participants = 8
            race1.totalKm = 32
            race1.totalCal = 34543
            race1.latitude = 51.543545
            race1.longitude = -0.135234
            race1.user = user
            race1.timeline = timeline
            race1.createdDate = Date()
            
            let race2 = Race(entity: raceEntityDescription, insertInto: self.managedObjectContext)
            
            if let img = UIImage(named: "bg.jpeg") {
                race2.backgroundImage = UIImagePNGRepresentation(img)
            }
            
            race2.title = "Some awesome place 2"
            race2.totalTime = "2d 11h"
            race2.participants = 3
            race2.totalKm = 42
            race2.totalCal = 45323
            race2.latitude = 51.623543
            race2.longitude = -0.134623
            race2.user = user
            race2.timeline = timeline
            race2.createdDate = Date()
            
            let race3 = Race(entity: raceEntityDescription, insertInto: self.managedObjectContext)
            
            if let img = UIImage(named: "bg.jpeg") {
                race3.backgroundImage = UIImagePNGRepresentation(img)
            }
            
            race3.title = "Some awesome place 3"
            race3.totalTime = "4d 18h"
            race3.participants = 6
            race3.totalKm = 28
            race3.totalCal = 23456
            race3.latitude = 51.234435
            race3.longitude = -0.137423
            race3.user = user
            race3.timeline = timeline
            race3.createdDate = Date()
        }
        
        do {
            try self.managedObjectContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
