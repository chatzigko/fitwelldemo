//
//  TimelineItem.swift
//  FitwellDemo
//
//  Created by Konstantinos Chatzigeorgiou on 09/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import Foundation
import UIKit

enum TransportType: Int32 {
    case bus
    case train
    case airplane
    
    var icon: UIImage {
        switch self {
        case .bus:
            return #imageLiteral(resourceName: "bus")
        case .train:
            return #imageLiteral(resourceName: "train")
        case .airplane:
            return #imageLiteral(resourceName: "airplane")
        }
    }
    
    var title: String {
        switch self {
        case .bus:
            return "Bus"
        case .train:
            return "Train"
        case .airplane:
            return "Airplane"
        }
    }
}
