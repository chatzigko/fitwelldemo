//
//  RaceViewController.swift
//  FitwellDemo
//
//  Created by Konstantinos Chatzigeorgiou on 08/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import BEMCheckBox
import TimelineTableViewCell

// MARK: - Properties

class RaceViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapMapView: MKMapView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var totalKmLabel: UILabel!
    @IBOutlet weak var totalCalLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var followingCheckBox: BEMCheckBox!
    
    var checkbox: BEMCheckBox!
    
    // MARK: - Properties
    
    var race: Race?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.followingCheckBox.boxType = .square
        self.mapMapView.delegate = self
        
        if let race = race {
            self.title = race.title
            
            let user = race.user
            
            if let userAvatarData = user?.avatar {
                self.avatarImageView.image = UIImage(data: userAvatarData)
            }
            
            let firstName = user?.firstName ?? ""
            let lastName = user?.lastName ?? ""
            self.fullNameLabel.text = "\(firstName) \(lastName)"
            
            self.totalTimeLabel.text = race.totalTime?.uppercased()
            self.totalKmLabel.text = "\(race.totalKm)"
            self.totalCalLabel.text = "\(race.totalCal)"
            
            let locationCoordinate2D = CLLocationCoordinate2D(latitude: race.latitude, longitude: race.longitude)
            let pointAnnotation = MKPointAnnotation()
            pointAnnotation.coordinate = locationCoordinate2D
            
            self.mapMapView.addAnnotation(pointAnnotation)
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(locationCoordinate2D, 250, 250)
            let coordinateRegion = self.mapMapView.regionThatFits(viewRegion)
            self.mapMapView.setRegion(coordinateRegion, animated: true)
            
            self.geocoder(location: CLLocation(latitude: race.latitude, longitude: race.longitude))
        }
        
        let customTimelineTableViewCellNib = UINib(nibName: "CustomTimelineTableViewCell", bundle: nil)
        self.tableView.register(customTimelineTableViewCellNib, forCellReuseIdentifier: "TimelineTableViewCell")
        
        self.view.layoutIfNeeded()
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2
    }
    
    // MARK: - Methods
    
    func geocoder(location: CLLocation) {
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location) { [unowned self](placemarks, error) in
            if let placemark = placemarks?.first {
                let city = placemark.locality ?? ""
                let country = placemark.country ?? ""
                
                self.locationLabel.text = "\(city), \(country)"
            }
        }
    }
    
}

// MARK: - MKMapViewDelegate

extension RaceViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = MKAnnotationView()
        annotationView.image = #imageLiteral(resourceName: "shape")
        annotationView.frame.size = CGSize(width: 40 * 51/66, height: 40)
        
        return annotationView
    }
    
}

// MARK: - UITableViewDataSource

extension RaceViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.race?.timeline?.items?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineTableViewCell", for: indexPath) as! CustomTimelineTableViewCell

        let item = self.race?.timeline?.items?[indexPath.row] as? TimelineItem
        
        cell.timeline.backColor = UIColor.lightGray
        cell.timeline.frontColor = UIColor.lightGray
        cell.timeline.width = 1.5

        if let type = item?.type {
            cell.illustrationImageView.image = TransportType(rawValue: type)?.icon
        }
        
        if let type = item?.type {
            cell.titleLabel.text = TransportType(rawValue: type)?.title
        }
        
        cell.descriptionLabel.text = item?.desc
        cell.timeLabel.text = item?.time

        return cell
    }
    
}
