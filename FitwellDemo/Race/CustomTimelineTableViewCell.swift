//
//  CustomTimelineTableViewCell.swift
//  FitwellDemo
//
//  Created by Konstantinos Chatzigeorgiou on 09/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit
import TimelineTableViewCell

class CustomTimelineTableViewCell: TimelineTableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var timeLabel: UILabel!
    
}
