//
//  RoundView.swift
//  FitwellDemo
//
//  Created by Konstantinos Chatzigeorgiou on 09/07/2018.
//  Copyright © 2018 Konstantinos Chatzigeorgiou. All rights reserved.
//

import UIKit

class RoundView: UIView {
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 4.0
    }
    
}
